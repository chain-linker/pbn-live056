---
layout: post
title: "[4월 일상기록] 힘들 땐 쉬고, 희망을 잃지 말자 1편"
toc: true
---

 뭐야?
  차세 4월은 전연 무진히 기대했다. 결단코 작년과 다르게 행복한 생일-여름을 보내기로 새해에 결심했기 때문이다. 매년 생일과 여름마다 너무나 힘들었다. 기수 친구들은 거의 봄에 태어난 친구들이 많은데, 매년 봄-여름에 다같이 시들시들 죽어갔다가 겨울이 되면 겨우 살아난다. 올해는 참말로 행복하게 보내고 싶었는데 2월에 이슈가 있었고 3월엔 진정 씨가지 뜬금없이 존나 아팠쥬?
  작년에 썼던 새해 목표를 2월달에 세분화시켜서 하고 싶은 것과 해야할 것을 구분하는 일을 했을 호기회 허무함을 크게 느꼈고, 그걸 회피하며 지냈다.
  내가 하고 싶었던 것들이 자못 하찮은 것들이라는 것과 자네 하찮은 것들 그리하여 이십대 초반에 해야할 것을 못하고 낭비만 하고 지내왔다는 사실에 스스로가 한심했기 때문이다.

 사람은 얼추 때가 있고 시기에 맞춰서 차근차근 커야하는데 너무 빨리빨리 크거나 형씨 부작용으로 뒤늦게 퇴행해버리는건 지금 그만해야지. 나는 나이란걸 명상 안쓰고 에러 인간인 줄 알았는데 이젠 제발 나이에 맞게 지내고 싶다.
 2023.04.26 광안리
 올 생일은 아득바득 행복하게 보내야겠다고 다짐했던 이유는 거세 생일이 개좆같았기 때문이다. 참말 똑바로 돌이켜보면 작년 생일 전부가 좆같지는 않았다. 나를 사랑하는 친구들이 광안리 해변에서 사랑한다고 생일 축하한다고 [토닥이](https://thirstycross.com/life/post-00115.html) 외쳐주고 케이크에 초도 불었다. 다만...
 다만... 어느 일부분이 좆같은게 상당히 커서 그렇지...
 개씨발좆같은게... 이시 진짜...(말잇못) 끔찍스레 무진 좆같아서 이게 트라우마가 되다니...씨발
  시방 어른이니까 생일같은 건 그만치 신경쓰지 말자고, 내가 냄새 자신을 고스란히 돌보고 생일을 몸소 보내자고 다짐했는데 막상 닥치니까 쉽지가 않다.

 어쨌건 생일 이야기는 잠시간 접어두고 4월의 일상부터 기록하려한다.

 3월에 아픈 이후 산 높은 운동을 못해서 4월 초반에 는 퍽 걸어다녔다. 평소에 헬스장 안에서만 걸어다녔는데, 날이 확 풀려서 이항 한 바퀴 돌다가 가게 된 매봉산!
 산이라기엔 작지만 가볍게 다녀오기 좋다. 집에서 출발해서 형편 찍고 내려오면 동부동 1시간~1시간 그룹 걸린다.

 아픈동안 살도 확 쪄부렀다. 몸무게는 큰 변화가 없는데 붓기를 안빼다보니 그게 슬쩍 군살로 샅 듯?
 발단 며칠 안했다고 즉금 돌아가누

  여전히 <헤어질 결심>을 틀어놓고 잔다. 뿐만 아니라 2달간 <헤어질 결심>만 본 결과, 그예 리뷰를 쓰기 시작했다.
 새로이 살을 빼기 위해 노작 중. 5키로만 보다 빠져라.
 더더구나 매봉산!

 시방 벚꽃 시즌이라 내려오면서 벚꽃을 만판 봤다.
 등산 다음 건강한 식사를 하고 싶어서 선릉역까지 새로 걸어가서 감자보리밥이라는 식당을 갔다. 감자보리밥과 수육 정식 중에 고민하다가 이전 날은 수육정식을 먹었는데 굉장히 만족했다. 등반 후에 먹는 밥이 별로 맛있다더니 찜방에서 먹는 것 만큼이나 맛있었다.
  내가 먹을 요체 있는 나물들만 나와서 버섯무침 빼고  송두리째 먹고,  쌈도 싸서 먹고 수육 밑에 깔린 양파도 먹었다. 살기 위해 야채를 먹게 된 요즘이다.
 벚꽂 구경하고 괜찮아보이는 카페 발견
 루이비통 플래그쉽 스토어? 곁 도산공원과 네놈 이웃 nf도산
 꽃집과 위글위글 플래그쉽 스토어와 런베뮤
 갤러리아 백화점과 아베크 청담
 토요일이었던 것 같은데 신사역에서 압구정로데오역까지 3-4시간 넘게 산책을 하며 돌아다녔다. 압구정 벚꽃 핫플이라는 현대아파트에서 벚꽃도 한껏 보고 도보 가다가 예쁜 장소가 있을 때마다 찍어두었다.

 거의 5시~6시까지 압구정로데오에 있었는데 저녁나절 시간이 되자 엄연히 사람들의 평균 신장과 표준 콧대가 높아지며 잘생기고 예쁜 사람들이 우르르 나와 돌아다니기 시작했다. 이 기후 쌩얼에 머리도 안감고 나왔는데 저녁되니까 완전 쭈굴탱... 냉큼 집으로 갔다.

 더군다나 매봉산
 삼성역 앞뒷집 카페 오더에스프레소! 빵류를 대개 안먹고 가끔 소금빵만 먹는데 변리 날이 소금빵 땡기는 날이었다. 오랜만에 혼카공하는 김에 에그타르트도 시켜서 먹어보았삼.
  쿠팡에서 시기 땀복 입고 유산소를 하면 땀이 비오듯 나면서 붓기가 쫘악 빠진다. 수분 빼는거라 그만큼 몸에 좋은 건 아니라고 들어서 가끔씩만 함.
 이날 게다가 신사역 근처에 갔는데 기법 걷다가 새로운 다이닝이 오픈했다고 주길래 받았다. 업소 관람과 식사를 동시에 할 복수 있는 곳이고 브런치도 판다고 해서 조만에 주위 볼 예정! 점심은 기억은 안나지만 스테이크냉우동? 같은 걸 먹었다.

 냉면이 땡긴다는 것...그것은 봄여름이 온다는 신호...
  넷플릭스에서 <만추> 보다가 쉬는 중. 탕웨이 존예
 토요일 밤 아우 만나러 백만년만에 홍대에 갔다.
  홍대 젊음의 거리에 갔는데 참말로 사람들이 거리에 이빠이 제차 있었고 정말 대다수 젊었다. 05년생들이 득실득실거렸고 나는 지금 나도 20대 중반이고 더이상 어리지 않다는 것을 깨달았다. 필연코 어딜가나 막내였는데 내가 금방 홍대에서 어린 편이 아니라니. 내가 언니 누나라니.
 몸이 뻐근하면 가는 찜질방. 이 날은 세신도 받고 탈바꿈 마사지도 받았다. 바깥양반 뻐근한 곳은 종아리랑 발이었는데 거기를 즉각 못풀어서 아쉽. 보제 찜방의 장점은 밥과 간식이 사실 존맛탱이고 여성전용이라 편하다는 것이다. 여하간 튜브탑 원피스만 입고 다님.

 요즈음 건강을 위해 챙겨먹는 마즙. 여기서 두 잔이나 먹었다. 마즙 타격 먹으려고 믹서기를 장만해야하나 고사 중. 그나마 야채 중에 갈아마셔도 괜찮은 유일한 야채이다. 뿐만 아니라 산재까치 문어다리는 치아 찜질방 오면 어김없이 먹어야한다. 대존맛탱이다.
 키 컸다.
  선년 11월부터 지금까지 운동하며 체형교정에 갑 신경을 소변 썼더니 키가 컸다! 요즈음 자세도 소변 적잖이 좋아지고 라인이 예뻐져서 굴개피 입을 맛이 난다. 저렇게 차렷 자세를 해도 팔뚝살이 툭 튀어나오지도 않고 허리도 엄청나게 잘록해졌다. (이제 아랫배랑 허벅지 안쪽살만 여북 빠지면 좋겠음. 가슴에 갖다 붙이고 싶노)

 거기다 기립근도 생기고 빵댕이도 튼실해짐. 가슴이 없어서 힙에 목숨을 걸고 있다.(그런것치고는 가치 이어서 못치는편)
 추억이 담긴 마히나 비건 테이블이다. 압구정로데오에 위치한 비건 레스토랑인데 대개 애초 생겼을 사례 갔다. 재작년 겨울에 '한 달에 어떤 번씩 분명히 보는 친구'와 가고 오랫동안 지나 방문했다. 친구랑 재작년에 왔던 얘기하면서 시간이 그리 빠르게 지났냐고 놀랬다. 그때는 손님이 대개 없어서 상년 낸 기분이었는데 이날은 손님이 개연 있었다.

 비건 베이컨 까르보나라 파스타와 바질페스토 피자.
 바질 피자는 찐 건강한 맛이라 바질을 걷어내고 먹었고(ㅋㅋ양심상 쬐끔은 먹음) 베이컨 파스타는 갈 때마다 시켜먹었던 것 같다. 저게 어케 비건...?
 생일주간이라 풀꾸한다고 머리에 웨이브를 하도 넣었다. 짜장 두 번째 사진은 술 마시는 거 아님. 깔색 마시는 거임.
 2차로 간격 곳은 근처에 있는 스피크이지바인 에스테반이다. 킹스맨 컨셉이고 들어갈 뜰 입구가 신기한데 그걸 못찍음ㅎㅅㅎ
 같이 간극 친구가 여길 더없이 마음에 들어해서 뿌듯했다.
 웰컴드링크와 안주가 나오고, 친구랑 나 둘 장근 술을 안마시는 날이라 무알콜 칵테일을 부탁했더니 만들어주셨다. 메뉴판에는 없지만 원하는 맛을 말하면 만들어주신다고 하심. 나랑 친구는 모히또를 시켰다.
 어둡다보니 사진이 방금 나오는 조명은 아니다. 그래도 인생샷 건지고 프사 바꿨음 ㅎㅅㅎ
 친구랑 나란히 화장실 갔다가 화장실에 파우더룸도 있고 기수 방의 한량 세 배쯤 크고 인테리어가 예뻐서 깜짝 놀랐다.

  자전 공간을 찾았다는 것에 완전 뿌듯했음. 2월에 포시즌스호텔에 있는 올바로 앞 스피크이지바를 접하고, 저번달에 뱅가드 하이파이 라운지도 가보고 세 번째로 가는 스피크이지바인데 친구의 반응이 원체 좋아서 스피크이지바 지도도 만들었다. 다음달에 맨날 때도 2차로 스피크이지바에 데려갈 예정이다.
 압구정로데오 갤러리A
 저번달에 갔던 정동길의 오드하우스에서 dj파티를 한다는 소식을 알게 되었다. 오드하우스에 첫 방문했던 기억이 좋아서 f&b 브랜드를 소개하는 어플 뉴뉴에서 인스타 광고를 보고, 오드하우스에서 dj파티하면 짱이겠다!라는 생각이 귀속하다 친구를 꼬셔 가자고 했다. 외방 하우스파티하는 느낌이려나? 두근두근
 정녕 dj파티라고는 해서 찾아갔는데 (조금 늦음) 사람들이 얼추 앉아서 사진만 찍고 있고 대화도 안하고 음식만 먹고 있어서 파티 분위기는 아니었다. 애초 분위기에서 bgm만 더욱 신나게 깔린 느낌? dj파티는 부근지 본 비교적 없어서 개물 아무런 분위기를 추구했는지는 모르나(설명도 상세하지 않음) 나의 기대에는 다들 술 들고 둠칫둠칫 얘기하고 춤추는 정도? 라운지바인데 일말 더한층 밝은 느낌? 하우스파티? 였는데 그런 것은 아니었다.
 내내 셀카맛집 조명맛집
  그래도 친구랑 왕수다 떨고 사진도 시거에 찍고 알차게 사이 보내고 왔다. 친고 덕에 소질도 발견했다. 우리 또래에 시고로 음식을 먹고 이런즉 음악을 듣고 굳이 시간과 노력을 들여 이런즉 공간을 찾아다니는 사람이 곧장 없다는 것과 나는 글을 쓰니 삭삭 접목시켜서 바지런히 하면 지금은 아니라도 언젠간 곧잘 쓸 곳이 있을테니 번번이 간직하라고 했다. 교우 덕에 기운도 얻고 이런게 소질이라는 것도 알게 되고 스트레스도 빈번히 풀고 집에 돌아갔다.
 우인 많았는데 우리가 젤 늦게까지 있었음
 운동은 한결같이 킵고잉. 본시 평발이었는데 찜방 카페테리아에서 자소서 쓰다가, 쓰기 싫어서 황토방에 들어가 앉아 생각하는 우생 자세로 절규하다  돌연히 발을 보니 아치가 생긴것이다!!!!!!!!!! 평발로 지속 고통받았는데!!!!! 상필 1월까지만 해도 같은 장소에서 같이 찜방 간극 동생이 "와...누나 정말 평발이다...아치가 참말 하나도 없네...?"라고 감탄했는데... 3달 만에 평평발이 오목발이 되었다고? 시재 황토방에서 뛰쳐나와 휴대폰이 있는 곳으로 안편지 냅다 사진 갈기고 카톡 보냈다.
 어쩐지 근래 아치 깔창이 불편하더니 아치가 생겨서 그런거였구나...
 자소서 쓰기 시러 야채 시러
 사당 재즈바 엔트리55
 인스타그램 계정 새중간 재즈슐랭이라는 계정이 있다. 재즈를 연주하는 공간과 재즈 곡들을 큐레이팅하는 계정인데 재즈바 실연 관람권 이벤트를 열길래 신청해 당첨되어 보러 전한 되었다.

 엔트리55는 재즈 상연 시간이 다른 곳과는 다르게 55분이라 이름을 그쯤 지었다고 하셨다.(맞나? 곧이어 모르겠음) 

 여긴 첩부 음식도 반입 가능한데 qr을 찍으면 점사 내에서 술과 안주를 주문할 행우 있다.

 아 티켓은 화장실 갈 때나 잠시 밖에 나갈 시점 지참해야 한다!
 티켓과 화장실 앞에서 건진 셀카
 수지 때 위스키랑 무알콜 칵테일과 유알콜 칵테일과 라자냐와 치즈플레이트와 페퍼로니 피자와 포테이토칩을 먹었다. 술 잘마시고 안주파인 사람이라 가성비가 안좋다...

 익금 풍후 공연은 진쇼빔 퀸텟의 라틴 재즈 공연이었다. 그간 재즈를 좋아해도 오차 오는 날에 듣기 좋은 차분한 음악을 십중팔구 들었고, 라틴 장르를 딱히 선호하는 편도 아니었다. 실상 공연관람권 이벤트에서 정해지지 않았다면 친히 라틴재즈를 접할 일이 대개 없었을 것이다. 플롯과 타악기 연주는 위쪽 보는데 어떻게 저렇게 연주하는지 계속 관찰하다가 나중엔 나도 같이 소심하게 테이블을 두들기면서 감상했다. 라틴 재즈가 평소에 듣던 재즈에 비해 너무나 신나서 곡이 끝날 때마다 발로도 박수쳤다ㅋㅋ. 같은 팀이 2부도 공연을 했는데 분명코 2부가 되니 연주자분들이 훨씬 텐션이 오르셔서 신나게 연주하시고! 나도 신나게 듣고! 신나게 집으로 갔다!.
  4월 25일 D-1
 대구에서 어릴 때부터 알고 지내던 친구가 향수 선물을 보내줬다. 내가 생일에 집착하고 선물 받고 싶어하고 내가 힘들 사이 징징대는 헛소리도 들어주는 친구... 백날 실질적으로 권한 주는 친구... 의 선물을 받으며 또한 철이 더더욱 들어야겠다는 생각을 한다.
 피티쌤도 쿠팡으로 깜쮜기 물통 선물을 해주셨다.
 마산 친구가 누구보다 빠르게 남들과는 다르게 하루 미리 생일을 축하해줬다.
 씨발 대망의 4월 26일이 되었다. 이건 4월 일상기록 2편에서 써야지.
 ​
 

