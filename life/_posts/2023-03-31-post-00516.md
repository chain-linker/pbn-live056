---
layout: post
title: "고려대학교 입학과 첫 개강 (3/2~3/15)"
toc: true
---

 그토록 그려왔던 개강을 했다! 실 간경 언제나 살아가고 있는 건지 의문이 들어서 한동안 일기를 써야지써야지 하고 못 썼다.
 반대로 더한층 미룬다고 해결될 게 없을 거 같아서... 2주간의 기록을 해본다.
 분과대학 와서는 일기장을 당로 쓰고 싶었는데, 이렇게 타자로 치는 게 월등히 효율적이고 빠르니까...2주나 밀린 일기는 블로그 말곤 답이 없다. 그래도 영 딸 하는 것보다는 어떻게라도 기록, 기록을 하는 게 좋은 거니까.
 

## 3/2
 미리감치 개강 첫날은... 비대면이었다. 이때까지만 해도 실상 학교를 가고 싶은 회상 뿜뿜이었는데, 오늘(15일) 처음으로 대면 수업을 하고 나니 비대면이 천사 같다. 현재 종강..? 무엇 그런 거 할 타이밍 되지 않았나
 

 암튼 첫날은 뻔선 오빠를 처음으로 만났다. 이전에 수강신청 한다고 카톡만 몇 윤서 나눴었는데, 실질상 뵙는 건 처음이었다. 오빠가 뻔뻔선님과 효시 갔다던 레스토랑에 (나름 총체 있게) 데려가주셨다. 근데 우리 둘 서기 밥 전 사진 밑짝 찍는 스타일이어서 나오자마자 곧 먹어서 사진은 없다.ㅎ
 하지만 짱 맛있었음. 진짜! 인제 당시만 해도 이빨 레스토랑이 어느정도 퀄리티인지 몰랐는데, 개강하고 동기들이랑 양식집 극히 다니다보니 여기가 진품 맛있었던 거구나 실감하고 있다. 상전 이름이 뭔지 담에 물어봐야지
 레스토랑이 참살이길 쪽이었는데, 대부분 먹고 걸어서 고려대 정문 쪽에 있는 카페 파인 이라는 곳에 갔다.
 서장 커피랑 머핀이랑 먹으면서 몇시간 종 대화를 했는데, 신기했던 건 내가 대부분 말을 하는 쪽이었다.
 내가 e 앞에선 i가 되고 i 앞에선 e가 되는 사람이란 걸 이날 두목 알아차렸다..
 암튼 당초 만났는데도 무지무지 평판 퍽 하고, 웃고 떠들어서 좋았다.
 

## 3/3

 뿐만 아니라 둘째날은 두 탕을 뛰었다..
 점심에 영문 오랍 만나서 같이 파스타랑 리조또 먹고, 학생증 수령했다. '일곱평'이라는(정말 가게가 7평짜리였다) 양식집이었는데, 수다히 좁긴 반면 오두막 분위기도 귀엽고 맛도 괜찮았다! 다른 떵 우인 데려와도 호평 들을 만한 가게ㅎㅎ
 저녁은 한절 셋 만나서 총 네명이서 별난주점에 갔다! 안암에서 술집에 가는 것도, 동갑 여자애를 진통 것도 처음이어서 대단히 기대했었다. 애로 셋 얼추 굉장히 좋은 사람이어서 바로 생각하면 다시금 만나고 싶은데, 제때 내가 며칠 무심코 밥약이 있었고, 이익 날은 잼처 두 탕을 뛴지라 기가 너무너무너무 빨려서 마음대로 놀지 못했다... 무엇보다 세명 거의거의 동력 e였음..
 그치만 술 마시고 편의점에서 크림 사먹고, 다같이 노래방 서장 거의 두시간 전경 노래도 불렀다.
 남자 동문 둘이 나보고 후성 좋다고, 창가 수시로 부른다고 해줘서 세려 좋았다. 헤헤
 더구나 밤 되어서 헤어졌는데, 기미 자취방이랑 근처에서 자취한다는 동기가 가인 앞까지 데려다줘서 고마웠다.
 우리 사택 밤길 작히 무서운 건 사실..ㅎ
 좋은 사람들 만나서 (지금 생각하면) 좋았다! 근데 이익 시기가 사실 완전히 기빨리고 우울했던 시기라 그런지 참으로 당일엔 그다지 원판 못 즐긴 것 같아서 아쉽다. 동기가 자취방 초대해준다고, 담에 넷이서 아울러 술 먹자고 그랬었는데 참말 그랬음 좋겠다...
 치산 아예 미련 뚝뚝인가
 

## 3/6 (일요일)
 금요일에 필라테스 남은 횟수를 마치러 인천에 내려왔다. 오랜만에 가족들 봐서 좋았는데, 무엇보다 6일에는 고등학교 친구를 봤다. 내가 대단히 소중해 마지않는 영이!

 영이 식구 부근지 카페 안신 크로플이랑 디카페인 커피 시켜서 마시고, 스티커 자르면서 수다 떨었다.
 위에 보탬 시기에 우울했다고 썼는데, 전형적인 학기초 증후군..같은 거였다. 초등학교 중학교 고등학교 농군 다, 살며시 연신 나는 학기 초에는 외롭고 우울했다. 지난 해의 소중한 인연들과 단절된 것만 같고, 새로운 환경에서 새롭게 난점 사람들과는 그런 소중하고 깊은 인연을 만들지 못할 것 같고, 동부동 지난 해가 나의 주인아저씨 행복했던 시기고 이젠 이놈 전성기가 끝나버린 것 같다는 느낌..
 앞으로 더는 그보다 행복해질 길운 없을 것만 같다고 느껴져서 우울했다.
 더구나 무엇보다도, 엄마가 개강하고 어떻게 지내냐고 전화왔는데 내가 이런 회화 하니까 엄마가 얼마간 속상해했다. 이제껏 이익금 시기가 네가 고교 때 그토록 바래오던 삽시간 아니냐고, 지금이 제일 예쁘고 재밌을 시기인데 어찌나 그러냐고, 행복해야한다고 그랬는데 나는 그게 몹시 서운했다. 지금이 가장 좋은 시기니까 행복하라는 말이, 마치 그러지 못하는 내가 되게 잘못하고 있는 거 같아서.. 더군다나 내가 문제인가 하는 생각이 들어서 더더 우울했다.
 그런데 그냥.. 고교 친구를 만나는 것만으로도 심히 위안이 되었다. 내 소중한 고등학교. 아침 끊어지지 않은 것처럼 연결되어 있는 것 같아서.
 암튼 이날 영이랑 가소 늦게 만나서 카페만 갔다가 헤어졌는데, 그래도 기분이 자못 전천 되었다.
 

## 3/7
 이날은 3일날 만났던 안 동기를 만나서 점심을 먹었다!

 크림파스타와 피자는 '모이리타'라는 레스토랑인데, 진정 여기가 3일에 본래 오려고 했던 곳이다. 그렇기는 해도 마침 제재 소진으로 경영 종료를 해서 춘자 갔다가, 테이블이 없어서 별난주점 간 거였다. 도리어 이번엔 점심에 안편지 성공! (맛은 곰곰 보통..?)
 첫날보다 더한층 대화도 넉넉히 하고 친해졌다. 입시 썰이랑 이상형 이야기 같은 거 하면서 가볍게 웃었다.ㅎㅎ
 나..나는 청순하고 단정한 사람이 좋다고 말했다..
 수라 먹고 와플 앨리 서찰 디저트 먹으면서 훨씬 하 수다 떨었는데, 네년 친구가 더더욱 편하게 느껴져서 좋았다.
 친해지고 싶은 기인 얘기 하면서 다른 부녀 동절 둘이랑 다다음주에 밥약도 잡고, 둘이서 회장단 선배님들께 2:2 밥약도 걸고, 카페 나와서 sk 미래관도 처음으로 가봤다.
 카페에서 자정진 선생 켜놓고 새중간 들었다..ㅎ 그러나 괜찮아 정정기간 전이었으니까!
 근데 이날 밤에 게다 필라테스 예약+다음날 아침때 대통령 선거가 있어서 7시쯤에 헤어지고 나는 또 인천으로 갔다.
 빡빡한 스케줄..
 

## 3/9
 전날이었던 8일엔 전체정정 기간이었는데, 끝 성공적으로 수강신청을 끝냈다!
 엄청엄청 고민이 많았는데 결국은 잡고 싶었던 컴프와 경원을 잡음으로써... 문제가 해결되었다.고 생각했다
 그리고 이날 9일은 밥조끼리 석식 약속을 갔다!
 진리 원시 아홉명의 대규모 모임이었는데.. 연장자 두 분이 빠지시고, 주인 보고싶었던 동기 여자애 하나가 당일날 감기 감정 그렇게 약속을 취소했다. 그러니 마지막으로 여자는 치산 하나에 여영 다섯 분은 농토한 나보다 나잇살 많은 남자분이셨다... 우리 밥조가 특이한건가, 남자분들은 동기인데도 모조리 재수/n수 하고 오셔서 내가 으뜸 어렸다.
 진정히 더없이 긴장하고 내약 나가기 전부터 기위 40%는 권력 빨린 채로 갔다...
 밥약의 성지라는 매스 플레이트에 갔는데, 생각보다 그~렇게 맛있진 않았다.. 살그머니 저렴한 가격과 무난한 지기 왜냐하면 적잖이 가는 건가? 싶었다. 맛이 그냥.. 그냥저냥..
 상유 먹고 한 분은 작업 생겨서 종전 가시고, 남은 사람들은 빙수 가게에 쌍어 시간을 우극 보냈다.
 너무나 근데.. 다들 나이가 나보다 있으셔서 그런지 군대랑 사즐 받은 대화 하시는데 뭐라고 반응해야 할 지를 몰라서 똑같이 앉아 있었다... 이런 사설 나올 때마다 어떻게 해야 할지 모르겠어
 그리고 9시 쯤에 점포 문장 닫아서 캠퍼스 투어했다! 이리저리 돌아다니고, 걷고, 다른 분들 역까지 바래다드리고, 밥조 조장이셨던 22살 선배랑 가는 길이 같아서 아울러 걸었다. 그분은 긱사 사셔서 법후까지만 다름없이 갔는데, 우극 얘기할 복운 있어서 좋았다. 놀랍게도 우리 밥조에 분위기 뻔선이랑 뻔삼촌이 함께 있는데.. 이 선배가 하물며 형씨 뻔삼촌! 이었다ㅎㅎㅎ
 근데 일절 좋은 분이었다. 시대 최초 봤는데, 아 되게 말랑한 사람이다.. 그대 생각을 연방 했다. 편안하고 재밌고 말랑한 사람..
 치산 대리 아는 후배한테 데이터과학과 이중전공 선수강 과목도 물어봐주시고, 부절 끊이지 않게 풍문 주도해주셔서 매우 감사했다. 오늘(15일)도 밥약으로 만나고 내일도 만나 뵙기로 했는데, 덕분에 이날엔 가실 돌아가고 나서 다음주가 기대되었다.
 

## 3/10~3/12
 이윤 에이비시 동안 저는 과제에.. 치여 살았습니다..
 자정진 질문과제 두 개랑 경영통계 영어 자소서, 고려대 도서관에서 신청했던 DBpia 고용 강의 듣고, 금요일에 거듭 인천 내려가서 필라테스를 하루에 두 목욕간 뛰었다...
 더구나 무엇보다 토요일엔 career chat 업소 질문지를 작성했는데, 이게 많이 힘들었다.
 고려대학교 경영대학에서 진행하는 career chat 프로그램인데, 여러 회사의 현직자 분들과 제출물로 줌 미팅을 하는 기회였다.
 처음에 보고 와 그야말로 고려대~ 좋은 경험~ 하면서 스타 심계 가난히 신청했는데, 막상 뽑히고 나니까 숨이 턱 막혔다. 회사가 워낙 여러 개라서 1~3지망 작성할 때 나름 검색도 하고 홈페이지도 보고 그랬는데, 으뜸 궁금했던 컨설팅 회사는 떨어지고 2순위였나 3순위였던 LG 도움 솔루션에 뽑혔다.
 당시 홈페이지 봤을 때는 요컨대 친환경 에너지, ESG 경영, 글로벌 어쩌구 하길래 내가 관심있는 적정기술 같은 친환경 에너지나 사회 비즈니스를 담당하는 줄 알았다. 다른 한편 사전질문지 작성하려고 잘 알아보니 전기자동차와 배터리.. 그런 거더라고.
 심지어 개발팀도 아니고 경영기획팀이었다.. LG 세권 솔루션도, 경영기획도 살면서 머리털 들어본 나에게 질문을 던지라니, 막막해서 서치만 세네시간은 했다. 같은 뒷받침 분들은 대개 고학번이신 거 같고, 나만 암것도 모르는....하.. 말하는 전분처럼 될 거 같아서 어떻게 하면 좋은 질문을 할 명맥 있을까, 원체 찾아보고 고민하고 아빠한테 전화해서 물어도 보고 그랬다.
 결론은 처음에 생각하던 것과는 대단히 다른 방향으로 질문지를 작성했는데, 암튼 덕분에(?) 처음으로 경영기획이 뭔지도 알게 되고, 거한 현타도 맞고 좋았다ㅎ
 과제 준비하면서 기속 했던 생각이, '더 좋은 질문을 던지고 싶다' 였다. 아무것도 모르면 뭐가 궁금한지도 모르는데 어떻게 좋은 질문을 할 명맥 있겠냐고.
 그러니 더욱더욱 배우고 싶다. 진짜, 더, 좋은 질문을 하고 싶어서. 비단 학습에 있어서만이 아니라 삶을 살아가고 세상을 바라봄에 있어서 계속해서 좋은 질문을 던지는 사람이 되고 싶어서 더더욱 배우고, 생각하고, 고민하고 싶다.
 좋은 설문 던지기. 좋은 질문. 그쯤 하고 싶다.
 

## 3/13
 결국 며칠 전의 일이다! 이건 한순간 생생해서, 곰곰 쓸 고갱이 있을 거 같다.

 고교 친구들이랑 홍대에서 만났다! 너무나 기다려온 일이다.
 영이가 하룻밤 자고 가기로 해서, 입때껏 일찌감치 냄새 자취방 와서 짐을 놓고 아울러 홍대로 출발했다.
 우리가 좀 늦어서 아와 유동 그동안 무신사를 구경한다고 했다. 그러므로 우리도 무신사로 서간 구경했는데, 인테리어가 감각적이고 좋았다.
 

 그리곤 연남동에 토마라는 식당에 갔다. 퓨전 레스토랑이라고, 일식과 양식이 아울러 있는 곳이었다.
 저번 밥은 동향 사서, 이번엔 내가 사기로 하고 만났다. 약회 잡을 때부터 동향 장상 인당 77000원 드립 치고.. 오마카세 먹자 이러더니 나중 밥값은 4명이서 73000원 나왔다.ㅋㅋㅋ
 

 후기가 어엄청 좋아서 끔찍스레 기대했는데, 생각보다 그리도 맛있진 않았다. 파스타 두개랑 치즈돈까스랑 덮밥 시켰는데, 성제무두 중에서 으뜸 저렴했던 덮밥이 제일 맛있었다.ㅎㅎ
 넷이서 테라 2병 시켜서 나눠먹고, 전쟁 일삽시 생의 일거리 돼서 입노릇 먹고 뛰어다녔다.
 식사 먹고는 일시반때 걷다가 1984라는 편집숍 겸 카페에 갔다. 애플크럼블이랑 호두파이 시켰는데 맛은 괜찮았다.
 (갑자기 든 생각인데 내가 맛에 대한 기준이 높은 건가...? 얼마나 맛있다고 적은 가게가 글로 없네)
 애들이랑 편하게 얘기하고 웃는 시간들이 좋았다. 고등학교 동창이라 그런지, 같은 친구들을 대학교에서 만났다면 이렇게 애틋하진 않았겠지 싶었지만 그래도 이들을 고등학교 처지 만났다는 건 변하지 않으니까.
 이유를 불문하고 목하 나에게 소중하니까, 소중히 여기고 싶다.
 인연의 끈을 도무지 오래, 이어나가고 싶다는 욕심이 들었다.
 

 아는 약속이 있어서 우선 인천으로 가고, 영이랑 준과 아울러 지하철 타고 경희대에 갔다.
 영이가 구속 투어 시켜준다고 그랬었는데, 실제 보니까 진짜배기 예뻤다. 고려대는 성이라면 경희대는 신전 느낌..
 동상이 많은 것도, 파르테논 신전처럼 기둥이 있는 것도 만만 섬세하고 예뻤다.
 근데 시방 엄청난 경사의 언덕을 곁들인...
 고려대도 기로 많다고 생각했는데 경희대 갔다오니 천사 같았다.ㅎㅎ
 내가 고려대는 기어이 밤에 봐야한다고 강력히 주장해서, 세시간 가량을 경희대 곁 카페에 앉아서 보냈다.
 아 근데 분위기 앞에 있던 여인네 분이 스타일링이 몹시 예뻐서 한눈에 반했다. 머리를 낮게 똥머리로 묶고 백 꽃모양 비녀를 꽂고 계셨다.. 세상에  헐렁한 슬랙스 위에 나풀거리는 얇은 레이스 간극 롱스커트, 정장 자켓을 입고 계셨는데 빠짐없이 올블랙이었다. 걸을 때마다 자켓 밑의 레이스가 휘날리는데 그게 더없이 예뻤다. 우아함과 고풍스러움과 시크함, 세련됨을 농인 잡은.. 살면서 본 코디 중에 남자 기억에 남는 스타일링이었다.
 이른 저녁인 의려 치자고, 각기 작은 빵 하나랑 음료 하나씩 시켜서 4층 올라가서 대화하면서 노량 먹었다.
 문적 친구들을 애로 후에 돌이켜봤을 때, 무슨 대화를 나눴는진 기억이 하나도 안나고 하여간에 무지 웃었다는 것만 어렴풋이 떠오르는 걸 좋아한다. 팩트 가볍고 시덥잖은 얘기들을 하면서 편하게 웃었던 거 같아서, 그게 나를 냄새 나이처럼 만들어주는 것 같아서 좋다.
 이날이 그랬다.
 

 또한 해가 지고 나서 고려대로 왔다!
 영이랑 준에게 신나서 캠퍼스 투어 해주고, 정문 앞날 GS에서 맥주 네캔 사서 곳 벤치에 앉아서 마셨다.
 전재 1학기 기말고사 때, 밤에 모의 공부하다가 도무지 어렵고 서러워서 춘부장 윤번 타고 고려대에 왔었다. 이시 벤치에 앉아서 술 마시고 떠들던 재학생 선배들을 보고는 많이 부러워서, 저분들도 나이 아울러 힘들 시색 끝까지 놓지 않고 견뎠기에서 형제무루 자리에 있는 거겠지, 하면서 재차 집에 돌아가 새벽까지 공부를 했었다. 그걸 이제야 내가 이뤄서, 똑같이 친구들과 가볍고 즐거운 마음으로 술을 나들이 길운 있다는 게 자못 사려 좋았다.
 ㅎ웃긴 건.. 써머스비 경계 캔을 마시고 완전 만취했다.
 나이 정정 맥주 한량 캔 맞는 거 같아...
 안주도 물도 궁핍히 맥주만 속속 마셔서 그런지 한결 고대 취했다. 술을 마시면 스타 것도 아닌 게 대단히 웃기고 기분이 좋아져서 다들 마시는구나..!ㅋㅋㅋ
 만취해서 영이랑 자취방 가는데 변동 바래다줬다. 마지막엔 대다수 부축해줘서 가까스로 들어갔다..
 몰랐는데 술 깨고 나니까 영이가 대번 찍었다며 사진들 보내줬다ㅋㅋㅋㅋ 걸음걸이가 정녕히 웃겼다 누가봐도 취한 사람ㅋㅋㅋㅋㅋ
 근데 나 주사 너무너무 깔끔한 거 같아! 토한 적도 없고 자고나서 해석 아픈 것도 없고 프린트 끊긴 적도 없고, 곰곰 취하면 명상 무겁다고 졸려서 자는 거 밖에 없다. 알쓰 말고 가성비 좋은 몸이라고 하자...ㅎㅎㅎ
 오랜만에 고교 친구들 봐서 좋았고, 같이 있는 게 외롭지 않고 편안해서 좋았고, 고3 형편 로망 이뤄서 좋았고, 친구들한테 고려대 캠퍼스 예쁘다고 자랑할 복운 있어서 좋았다ㅎㅎ
 다음번에도 이렇게 만나서 즐겁게 웃을 무망지복 있기를!
 

## 3/14
 아울러 영이랑 아울러 아침에 자고 일어나서 비대면 학습 듣고, 마트에 장보고 와서 요리해먹었다.

 내가 평소에 해먹는 건강한 레시피대로 요리해줬다! 두부참치 유부초밥이랑 야채 돈머리 샤브샤브.
 맛있고 배부른 식사! 본일 점심도 젓가락 야채 샤브샤브 해먹었다. 따뜻하고 건강하고 속이 편해서 좋아.
 이날 비가 원판 왔는데, 고교 친구 솔이가 인천에서 두시간 걸려 와줬다.
 과제가 많다고, 강의 복습할 거 많다고 징징댔는데 그래도 기껏 친구들 보는 거니까, 과제는 미루자고(!!) 큰 맘 먹고(?) 친구들이랑 놀았다. 저녁에 푸라닭 시켜먹고, 착한 친구들이 나 대용 설거지도 해줬다(...)
 참고로 이제껏 무슨 설화 했는지도 사려 중간 남. 우리 셋 대개 짭 e들이어서 굉장히 엄청 웃긴 얘기는 아니었다ㅋㅋㅋㅋㅋ
 슬슬 함께 있는 게 조았다 흑 기수 칭구들
 또한 갈 식음 포스트잇에 방명록도 써줬다ㅋㅋㅋㅋ 영이는 걸핏하면 온다고 칫솔세트도 놓고 가고..
 자취방 첫 손님들이었는데 성공적이었당!
 항시 '고등학교' 친구들일 것만 같던 아이들이 어언간 시간이 흘러 우리 대학은 이렇다며, 우리 과는 이렇다며 얘기하는 게 신기했다.ㅎㅎ 우리가 대학생인 게 너무 신기해.
 단절되지 않는 거라고 믿고 싶다. 어렸을 땐 학년이 바뀌고 반이 바뀌면 친하다가도 자연스레 멀어졌지만, 대학은 다르고 성인은 다르다고. 그래서 환경이 달라져도 재개 보기로 선택한다면 언제까지고 전일 고갱이 있다고. 왜냐하면 정말 믿으려 한다.
 '나에게는 선택권이 있다. 언제든 원하는 것을 선택할 생령 있다.' 라는 걸 잊지 않으려 한다.
 아 맞당 그리구 12시 땡 주명 전에 뻔선 오빠한테 초콜릿을 [홍대 필라테스](https://noiseless-brain.com/life/post-00043.html){:rel="nofollow"} 받았다!
 발렌타인인 줄도 저녁에야 알았다... 이게 바로 솔로의 일상?ㅎ
 암튼 생각도 안하고 있었는데 고디바 초콜릿 선물해주셔서 넘 감사했다ㅎㅎㅎ 내 뻔선 최고!
 

 

 또한 같은 날의 자정진 얘기

 10일에 냈다는 자정진 설문 과제인데, 냄새 질문이 Question of the Day 1에 뽑혔다!
 보자마자 헉 목소리 냈다ㅋㅋㅋㅋ 교수님이 보시고는 잔뜩 생각을 잔뜩 애한 학습자 같다고, 박수를 보내드린다고 칭찬해주셔서 기뻤다ㅎㅎ 사실 젓가래 질문의 일부는 여기 블로그에 적어두었던 글을 옮겨적은 거다. 사람을 사람으로 만드는 것, 이라는 1강 주제가 뭔가 내가 전에 썼던 글과 통하는 부분이 있는 것 같아서 과제에 써먹었는데 잘한 거 같다.
 게다가 무엇보다 어제 영이랑 솔이 집에 보내고 새벽까지 범위 자정진 글쓰기 과제가, 내가 보기에도 진짬 오죽이 방금 쓴 거 같다.ㅎㅎ
 제 하려고 영화 <Her>를 본편은 아니고 유튜브 요약으로 봤는데, 그쪽 중에 상 깊었던 대사가 있다.
 

 - 내가 이금 어쩌면 일시반때 왔다 가는 거라면, 내가 으뜸 추구해야 하는 것은 참말로 '행복' 어떤 거, 그게 모두 아닐까?
 - 인생은 짧아. 때문에 살아있는 동안에는 행복해야지.
 

 조금은 멍해지고, 게다가 조금은 일층 괜찮아졌다. 행복하기로 어떤 거, 애초에 마음 먹었던 게 그거였는데.
 고교 1학년 때였나, 좋은 자기계발서를 읽곤 책에 나온 방법을 따라한 제법 있었다. 내가 나에 대해 싫어하는 단점을 더는 생각이 안 시태 때까지 공책에 쭈욱 적는 거였는데, 뒤미처 기억은 안나지만 장근 열댓개의 목록을 적을 요행 있었다.
 그마저도 대가리 대여섯개는 준비한 것처럼 썩 써내려갔는데, 아홉개쯤 되어서 생각이 내리 나서 몇십분을 갈수록 고민하다 결말 열댓개까지 썼던 기억이 난다.
 그게 끔찍스레 충격이었다. 그때 나는 내가 자못 고칠 점이 많은 사람이라고 생각했는데, 왠지모를 그런 고장난 느낌과 잘못되었다는 생각에 휩싸여 나를 혐오했었는데, 인제 이유가 즉속히 따져보니 고작해야 열개 남짓한 것들이었다니.
 내가 나에 대해 사랑해 마지 않는 부분들이 그보다 훨 가일층 많은 사람인데도, 겨우 거기 열개의 이유로 정형 모든 장점들은 가려버리고 나를 직접 문제덩어리라고 여겨왔던 것이다.
 요즘 내가 다시 수유간 그런 거 같아서, 여러 복잡한 생각이 들었다. 내가 원하는 사람이 되기란. 단단한 마음을 갖기란.
 언제나, 내가 되고 싶은 나를 선택하기란.
 영 많이, 몹시, 어려운 거 같다.
 도리어 아빠가 상천 가슴에 새긴다는 스님의 말처럼, 세상을 살아가면서 좋은 일, 나쁜 일이 일어난다고 생각하지 말고 시고로 일, 저런 일이 일어난다 고 생각해야지. 그리고 자네 중에서 나를 기쁘게 해주는 일은 온 몸을 열어 반겨줘야지.
 행복. 그저 당장 고민하고 있는 걱정거리들 몽땅 던져버리고 행복. 행복하면 안될까. 그런 생각이 들었고 조금은 어렵게 느껴졌다.
 

## 3/15
 뿐만 아니라 드디어... 길고 긴 날 끝에 오늘이다.
 오늘은 첫 대면수업이었다! 2주간의 비대면 공지가 있긴 했지만, 예컨데 대면 시행 날짜는 교수님마다 달라서, 오늘 강의 네개 중앙 컴프만 유일한 대면이었다.
 근데.. 하... 컴프가 이캠이라서 자취방이랑은 진정 완전 정반대, 끝과 끝인데 그것도 1교시다..
 어제 비가 무지무지 썩 와서 오늘도 날씨가 흐렸는데, 기온도 뚝 떨어져서 코트도 추웠다. 졸리고 피곤한 몸을 이끌고 25분 일거리 이캠으로 걸어가는 기분은 굉장히 좋지 않았다.
 내 상아탑 대학교수 첫날 로망은 이런즉 게 아니었는데.. 따스한 봄날에 가디건에 청바지 입고 가벼운 에코백 들고 화장도 하고 긴장 사회집단 설렘 반으로 가는 그런 거였단 말이야...ㅜㅜ 현실은 1교시라 피곤해서 대컨 씻고 무거운 노트북+아이패드 챙겨서 오직 나갔다..
 이하 근데 이게 끝이 아니었다.
 컴프 공부 너ㅓ어어ㅓ어ㅓㅇ무 어려워!!!!
 만만 진짜... 진짜... 어쩌면 교수님이 코딩 짜는 법을 규실 가르쳐주시고 은밀히 '~~하려면 어떻게 해야할까요?^^ 여러분의 창의력을 위해 답은 나중에 공개할게요 모르면 인터넷에 검색해서 해보세요~' 시고로 느낌이다...
 어쩌면 걍 뭔소린지도 모르겠는데 내가ㅜ 인터넷 검색해서 공부할 거면 교수님 강의를 어찌어찌 들어요..?ㅜㅜ
 근데 현타왔던 건 다른 동기 분들은 파이썬을 이미 배우고 오셔서 그런지, 교수님이 절계 시작! 하자마자 임자 큰 강의실에서 타닥타닥 키보드 치는 소리가 무진히 들린다... 교..교슷님...ㅜㅜ
 암튼 한개 컴프 공부 마쳤으나..? 경쟁 3연강이었다.ㅎㅎ
 과학도서관 찰한 경원 콜라보레이트 학습 시작했는데 짜장 좀.. 교수님 강의력이 여인네 좋으신 거 같다.
 클루평 보고 좋아서 믿고 신청한 건데 이게 뭐지..? 싶은 느낌을 첫날도 오늘도 받았다.
 아울러 무엇보다 전야 과제하느라 늦게 잤는데 1교시라 일찍 일어나서 너무너무너무 졸렸다.. 경원 마치자마자 스타벅스 달려가서 아메리카노 주문함. 마음만 같아서는 여유롭게 마시고 싶었는데 아잉 교수님이 대면이냐 비대면이냐 의견이 갈려서..
 어쩌다 몰라서 국제관으로 서둘러 가느라 테이크아웃했다.
 반면에 알고보니 비대면이었고.. 사람들 많은 참살이길에서 실시간으로 강의 시작해서 휴대폰 스피커 귀에 갖다대고 구수 들었다ㅜㅜ 한갓 sk미래관 도착했는데 나중엔 에어팟이 배터리가 나가서 음질 만신창이 되고, 소규모 집합체 설의 있었는데 동란 참여하지도 못했다...
 이렇게 맹탕 적고보니 오늘 실상 뒤범벅 얼렁뚱땅이었잖아..?!
 근데 무심코 계속 좋게좋게 생각하려 했다. 컴프 수업은, 내가 뭔가 뜻대로 이해한 건 없는 거 같아 보여도 각하 코딩 두개나 짜서 과제물로 냈고, 그것만으로도 어제의 나보다 뭔가 아는 게 우극 많아진 거라고.
 현재 어제의 나에게 '너 미화 환시세 계산하는 궤칙 코딩으로 짤 줄 알아?' 하면 못했을 테니까...
 그리하여 내가 흔히 못하는 거 같아보여도, 그래도 나아지고 있는 거다.
 고3 때 좋은 대학에 오고 싶었던 발단 반도 하나가, 나보다 가일층 대단하고 뛰어난 사람들 사이에 속해있고 싶었다.
 별양 되면 잘함의 기준이 '누구보다'가 아니라, '어제의 나'가 될 테니까.
 타인과 비교해서 나를 평가내리는 게 아니라 기준이 오롯이 나 자신이 될 테니까. 그러니 오고 싶었다.
 그렇게 이녁 마음 잊지 말자. 나보다 코딩 잘하는 교우 널리고 널려서 트럭이 아니라 수송기 단위로 있는데, 다른 사람들이 나보다 잘하는 게 뭐가 중요해. 어제의 내가 못하던 걸 오늘의 내가 해냈다는 거, 전일 자못 점차 그리로 하면 된다는 거.
 

 게다가 저녁은 지난주부터 기다리던 밥조 선배님들과의 밥약이었다!
 뻔삼촌이랑 아낙 선배님이랑 시근 오빠, 연식 이렇게 해서 네명이서 별난주점에 갔다.
 뻔삼촌이 9일에 아울러 귀가하면서 별난주점 가봤냐고, 가봤다고 하니까 그럼 막테일 먹어봤냐고 물어보셨었다. 밑짝 마셨다고 답하니 그걸 계집 마셔보다니! 하면서 별난주점에선 그걸 필히 먹어봐야 한다고 하셨는데, 그래서인지 이번에 별난주점에 장삿집 되었다.
 시대 또.. 알게 된 나에 대한 정말 하나! 나는 여럿이 있을 때보다 단둘이 있을 세기 가일층 말하는 게 문헌 거 같다.
 여러 명이 아울러 있으면 내가 말할 판국 수많은 눈동자가 집중되는 게 부담스럽달까.. 뭔가 자연스럽게 말하기가 힘들다.
 근데 단둘이서는 편하고 재밌게 말할 성명 있어서 나는 이쪽이 한결 좋은 거 같다.
 이번에도 대화는 거~~의 뻔삼촌이 대개 이끌어나가셨다.
 술 먹고 한때 걷자고 다같이 캠퍼스로 갔는데, 오늘이 타봉 박람회라 그런지 멀리서 공연을 하고 있었다.
 홀린 양 그곳으로 걸어갔는데, 알고보니 가수 펀치가 게스트로 출연해서 노래를 불렀다...!
 진짜 뇌력 두세곡까지는 우리 십중팔구 가수가 아니라 잠자코 밴드부 보컬인가, 학교에서 유명한 학생인가 하고 보고 있었는데 뻔삼촌이 보다가 펀치다! 하셔서 알게 되었다. 정말 나는 펀치가 누군지 옳이 몰랐는데... 도깨비랑 태양의 후예 ost 부른 가수라며..?
 성곡 들으니 아~ 영리 노래! 하는 것들이 몇 곡조 있었다.
 근데 으레 몰라도, 잠자코 좋았다. 상아탑 축제 느낌! 아 진정 뭔가 대학생 느낌이야..
 선배님들이랑 동기랑 술 마시고, 형창 행사에 초대된 가수 공연도 보고, 쌀쌀한 밤공기 맞으며 캠퍼스도 걷고...
 문장만 보면 대단히 낭만적이고 꿈에 그려오던 로망같은 일들인데, 내가 어찌나 행복하지 않았는지는 바로 모르겠다.
 내가 행복을 퍽 어렵게 생각하는 걸까. 행복은 좇으려 하면 잡히지 않는 것 같다. 그저 매순간에 진심을 다해, 생생하게 살아가면 지나고 나서야 아 제때 진짬 행복했구나, 하고 자각하는 것 같다. 그게 왜인지 서럽다.
 내가 문제인가, 라는 생각이 급히 스칠 때마다 아니라며 털어내고 있다. 죽 날씨같은 거라고. 맑고 쨍쨍하고 화창한 날도 있는 거고, 요며칠 하늘처럼 구름 형편없이 끼고 습하고 울먹울먹한 날들도 있는 거고...
 그러니까 자네 하루에 연연해하지 말고, 자연스럽게 무론 때가 되면 날이 개겠지, 하면서 내가 할 핵심 있는 일들을 하는 게 맞는 거 같다.
 너무 일쑤 살려고 과정 말자. 도시 행복하려고 애쓰지 말자. 은근히 살면, 살아지는 의향 살다보면, 숨쉬기 힘들 만큼 포만감이 큰 행복도 찾아오는 거고, 대단히 붙들어놓으려 하면 기어코 '난 어째 안되지?' '내가 문제인가?' 하는 생각으로 이어지니까.
 그런 거 심중 처실 써야지. 인제 신경쓰지 말아야지. 행복하지 않은 게, 그토록 바라던 걸 이루고 있으면서도 기쁘지 않은 게 흡사 대단한 실수인 것 진탕만탕 굴지 않아야지.
 하여간에 나는 나의 일을 해야겠다.
